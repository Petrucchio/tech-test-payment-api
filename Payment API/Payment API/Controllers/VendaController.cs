using Microsoft.AspNetCore.Mvc;
using Payment_API.Models;
using Payment_API.Context;

namespace Payment_API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly VendaContext _context;

        public VendaController(VendaContext context)
        {
            _context = context;
        }

        [HttpPatch("Atualizar Status")]
        public IActionResult ObterPorStatus(int id ,EnumStatusVenda status)
        {
            var venda = _context.Vendas.Find(id);
            switch (venda.Status)
            {
                case 0: //pedido aguardando pagamento
                    switch (status)
                    {
                        case (EnumStatusVenda)1: //alterar para pagamento aprovado
                            venda.Status = status;
                            _context.Update(venda);
                            _context.SaveChanges();
                            return Ok("Status alterado para (Pagamento Aprovado)");
                            break;
                        case (EnumStatusVenda)4: //alterar para cancelado
                            venda.Status = status;
                            _context.Update(venda);
                            _context.SaveChanges();
                            return Ok("Status alterado para (Cancelada)");
                        break;
                        default: //invalido
                            return BadRequest(new { Erro = "O status da venda (Aguardando Pagamento) N�o pode ser alterado para o status solicitado" }); 
                        break;
                    }
                break;
                case (EnumStatusVenda)1: //pedido aprovado
                    switch (status)
                    {
                        case (EnumStatusVenda)2://alterar para Enviado para Transportadora
                            venda.Status = status;
                            _context.Update(venda);
                            _context.SaveChanges();
                            return Ok("Status alterado para (Enviado para Transportadora)");
                        break; 
                        case (EnumStatusVenda)4://alterar para cancelado
                            venda.Status = status;
                            _context.Update(venda);
                            _context.SaveChanges();
                            return Ok("Status alterado para (Cancelada)");
                        break;
                        default: //invalido
                            return BadRequest(new { Erro = "O status da venda (Pagamento Aprovado) N�o pode ser alterado para o status solicitado" });
                        break;
                    }
                break;
                case (EnumStatusVenda)2://pedido Enviado para Transportadora
                    switch (status)
                    {
                        case (EnumStatusVenda)3://alterar para Entregue
                            venda.Status = status;
                            _context.Update(venda);
                            _context.SaveChanges();
                            return Ok("Status alterado para (Entregue)");
                        break;
                        default: //invalido
                            return BadRequest(new { Erro = "O status da venda (Enviado para Transportadora) N�o pode ser alterado para o status solicitado" });
                        break;
                    }
                break;
                default:
                    return BadRequest(new { Erro = "O status da venda n�o podem ser alterados porque a venda foi entregue ou cancelada" });
                   break;
            }

            
            return Ok(venda);
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var venda = _context.Vendas.Find(id);
            venda.Vendedor = _context.Vendedors.Find(venda.Vendedorid);
            var produtolist = _context.Produtos.ToList().Where(x => x.vendaId == venda.Id);
            venda.Produto = produtolist.ToList();
            if (venda == null)
                return NotFound();

            return Ok(venda);
        }

        [HttpPost]
        public IActionResult RegistrarVenda(Venda venda)
        {
            if (venda.Data == DateTime.MinValue)
                return BadRequest(new { Erro = "A data da venda n�o pode ser vazia" });
            if(venda.Produto.Count == 0)
                return BadRequest(new { Erro = "Ao menos um produto precisa estar cadastrado" });

            _context.Add(venda);
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterPorId), new { id = venda.Id }, venda);
        }
    }
   
}