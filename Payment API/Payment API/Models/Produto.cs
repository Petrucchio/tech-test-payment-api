﻿using System.ComponentModel.DataAnnotations;

namespace Payment_API.Models
{
    public class Produto
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Nome requerido")]
        public string Nome { get; set; }
        [Required(ErrorMessage = "uma qunatidade precisa ser inserida")]
        public int Quantidade { get; set; }
        [Required(ErrorMessage = "Um preço faz-se necessario")]
        public float Preco { get; set; }
        public string Observacao { get; set; }
        public int vendaId { get; set; }

        Produto() { }

        public Produto(int id, string nome, int quantidade, float preco, string observacao)
        {
            Id = id;
            Nome = nome;
            Quantidade = quantidade;
            Preco = preco;
            Observacao = observacao;
        }
    }
}
