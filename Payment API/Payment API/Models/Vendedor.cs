﻿using System.ComponentModel.DataAnnotations;
using static Payment_API.Controllers.ValidacaoCustomizada;

namespace Payment_API.Models
{
    public class Vendedor
    {
        public int id { get; set; }
        [Required(ErrorMessage = "CPF requerido")]
        [ValidarCPf(ErrorMessage = "CPF Invalido")]
        public string Cpf { get; set; }
        [Required(ErrorMessage = "Nome requerido")]
        public string Nome { get; set; }
        [Required(ErrorMessage = "Email requerido")]
        [EmailAddress(ErrorMessage = "Email Invalido")]
        public string Email { get; set; }
        [Phone(ErrorMessage = "Numero de Telefone Invalido")]
        public string Telefone { get; set; }

        public Vendedor(int id, string cpf, string nome, string email, string telefone)
        {
            this.id = id;
            Cpf = cpf;
            Nome = nome;
            Email = email;
            Telefone = telefone;
        }
    }
}
