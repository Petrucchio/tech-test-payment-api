﻿using Microsoft.VisualBasic;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Payment_API.Models
{
    public class Venda
    {
        public int Id { get; set; }
        public int Vendedorid { get; set; }
        [Required(ErrorMessage = "É necessario ter um Vendedor")]
        public Vendedor Vendedor { get; set; }
        [Required(ErrorMessage = "É necessario ter um Produto")]
        public List<Produto> Produto { get; set; }
        [Required]
        public EnumStatusVenda Status { get; set; }
        public DateTime Data { get; set; }
        public string identificador { get; set; }


        public Venda(int id, Vendedor vendedor, List<Produto> produto, EnumStatusVenda status,DateTime data)
        {
            Id = id;
            Vendedor = vendedor;
            Produto = produto;
            Status = status;
            Data = data;
        }

        public Venda()
        {

        }

    }

   
}
